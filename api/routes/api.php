<?php

use App\Http\Controllers\ChapterController;
use App\Http\Controllers\StoryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/login', [UserController::class, 'login']);

Route::post('/register', [UserController::class, 'register']);
Route::group(['middleware' => 'auth'], function ($router) {
    Route::post('/logout', [UserController::class, 'logout']);
    Route::post('/refresh', [UserController::class, 'refresh']);
    Route::get('/user-profile', [UserController::class, 'userProfile']);
    Route::post('user/update/{id}', [UserController::class, 'update'])->name('user.update');

    // Story
    Route::group(['prefix' => 'story', 'as' => 'story'], function () {
        Route::get('/', [StoryController::class, 'index'])->name('index');
        Route::post('/store', [StoryController::class, 'store'])->name('store');
        Route::post('/all', [StoryController::class, 'storeAll'])->name('store-all');
        Route::get('/get-chapter-by-story/{id}', [StoryController::class, 'getChapterByStory'])->name('get-chapter-by-story');
        Route::get('/search/{keyword}', [StoryController::class, 'search'])->name('search');
        Route::get('/hot', [StoryController::class, 'hot'])->name('hot');
        Route::get('/news', [StoryController::class, 'updateNew'])->name('news');
        Route::get('/full-chapter', [StoryController::class, 'fullChapter'])->name('full-chapter');
        Route::get('/comments/{id}', [CommentController::class, 'getComments']);
        Route::get('/comments', [CommentController::class, 'store']);
    });

    // Chapter
    Route::group(['prefix' => 'chapter', 'as' => 'chapter'], function () {
        Route::get('/', [ChapterController::class, 'index'])->name('index');
        Route::get('/show/{id}', [ChapterController::class, 'show'])->name('show');
        // Route::post('/store', [ChapterController::class, 'store'])->name('store');
        Route::post('/all', [ChapterController::class, 'ChapterAll'])->name('chapter-all');
    });

    Route::group(['prefix' => 'category'], function() {
        Route::get('/', [CategoryController::class, 'index']);
        Route::post('/', [CategoryController::class, 'store']);
        Route::post('/all', [CategoryController::class, 'storeAll']);
        Route::post('/update', [CategoryController::class, 'update']);
        Route::delete('/delete/{id}', [CategoryController::class, 'destroy']);
        Route::get('list-stories/{id}', [CategoryController::class, 'getListOfStoriesByCategory']);
    });
});

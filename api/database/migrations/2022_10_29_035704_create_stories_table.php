<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stories', function (Blueprint $table) {
            $table->id();
            // $table->bigInteger('category_id')->unsigned();
            $table->string('name');
            $table->string('alias')->comment('or slug')->nullable();
            $table->string('author');
            $table->string('source')->nullable();
            $table->text('image')->nullable();
            $table->float('rating')->comment('đánh giá')->default(0);
            $table->integer('reviews')->comment('số lượt đọc')->default(0);
            $table->boolean('status')->default(true);
            $table->tinyInteger('intro')->nullable();    
            $table->longText('description')->nullable();
            $table->timestamps();
            // $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stories');
    }
}

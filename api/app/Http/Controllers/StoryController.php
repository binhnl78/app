<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Story;
use DB;
use Illuminate\Http\Request;
use Str;
use Weidner\Goutte\GoutteFacade;

class StoryController extends Controller
{

    protected $POST = ['story' =>
        ['category_id',
            'name',
            'alias',
            'author',
            'source',
            'image',
            'rating',
            'reviews',
            'status',
            'intro',
            'description',
        ],
    ];

    public function index()
    {
        try {
            $data = Story::all()->toArray();
        } catch (\PDOException $e) {
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
        }

        return $this->respond(200, 'OK', $data);
    }

    public function storeAll(Request $request)
    {
        DB::beginTransaction();
        try {
            $url = $request->url;
            if (!empty($url)) {
                $getDataByUrl = GoutteFacade::request('GET', $url);
                $urlStories   = $getDataByUrl->filter('h3.truyen-title')->each(function ($urlStory) {
                    return $urlStory->filter('a')->attr('href');
                });
                $category_alias = Str::slug($this->crawlerData($getDataByUrl, 'h1 span[itemprop="name"]'));
                $category_id    = Category::where('alias', $category_alias)->pluck('id')->first();
                Category::where('id', $category_id)->update(['description' => $this->crawlerData($getDataByUrl, 'div.panel-body')]);
                if (!empty($urlStories) && $category_id) {
                    foreach ($urlStories as $link) {
                        $urlRequest = GoutteFacade::request('GET', $link);
                        $data       = [
                            'category_id' => $category_id,
                            'name'        => $this->crawlerData($urlRequest, 'div.col-truyen-main h3'),
                            'alias'       => Str::slug($this->crawlerData($urlRequest, 'div.col-truyen-main h3')),
                            'author'      => $this->crawlerData($urlRequest, 'div.info a[itemprop="author"]'),
                            'source'      => $this->crawlerData($urlRequest, 'div.info span.source'),
                            'image'       => $this->crawlerData($urlRequest, 'div.book img', 'src'),
                            'rating'      => $this->crawlerData($urlRequest, 'div.rate span[itemprop="ratingValue"]'),
                            'reviews'     => $this->crawlerData($urlRequest, 'div.rate span[itemprop="ratingCount"]'),
                            'status'      => (Str::slug($this->crawlerData($urlRequest, 'div.info-holder div.info div:last-child span')) == 'dang-ra') ? true : false,
                            'intro'       => 1,
                            'description' => $this->crawlerData($urlRequest, 'div.desc-text'),
                        ];
                        Story::updateOrCreate($data);
                    }
                    DB::commit();
                }
            }
        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
        }
        return $this->respond(200, 'OK', []);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            // $stories = new Story();
            // foreach ($this->POST['story'] as $story) {
            //     $stories->$story = $request->$story;
            // }
            // upload
            // $imageName = '';
            // if (isset($request->image)) {
                $imageName = time() . '.' . $request->image->extension();
                $request->image->move(public_path('images'), $imageName);
                // $stories->image = $imageName;
            // }
            // $stories->save();
            // DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
        }
        return $this->respond(200, 'OK', ['image' => $imageName]);
    }

    public function getChapterByStory(Request $request, $id)
    {
        $chapters = Story::findOrFail($id);
        $items[]  = $chapters->chapter->toArray();

        return $this->respond(200, 'OK', $items);
    }

    public function hot()
    {
        $items = Story::latest('rating')->take(6)->get()->toArray();
        $msg   = 'Error get data';
        if (!$items) {
            return $this->respond(404, 'NG', ['message' => $msg]);
        }
        return $this->respond(200, 'OK', $items);
    }

    public function updateNew()
    {
        $items = Story::latest('updated_at')->take(6)->get()->toArray();
        $msg   = 'Error get data';
        if (!$items) {
            return $this->respond(404, 'NG', ['message' => $msg]);
        }
        return $this->respond(200, 'OK', $items);
    }

    public function fullChapter()
    {
        $items = Story::where('status', 0)->latest('created_at')->take(6)->get()->toArray();
        $msg   = 'Error get data';
        if (!$items) {
            return $this->respond(404, 'NG', ['message' => $msg]);
        }
        return $this->respond(200, 'OK', $items);
    }

    public function search(Request $request)
    {
        $query = Story::where('name', 'like', '%' . $request->keyword . '%')->orWhere('author', 'like', '%' . $request->keyword . '%')->get()->toArray();
        dd($query);
    }

    // if (!empty($url)) {
    //             $urlRequest          = GoutteFacade::request('GET', $url);
    //             $data['name']        = $this->crawlerData($urlRequest, 'div.col-truyen-main h3');
    //             $data['slug']        = Str::slug($this->crawlerData($urlRequest, 'div.col-truyen-main h3'));
    //             $data['alias']       = $this->crawlerData($urlRequest, 'div.info a[itemprop="author"]');
    //             $data['content']     = $this->crawlerData($urlRequest);
    //             $data['source']      = $this->crawlerData($urlRequest, 'div.info span.source');
    //             $data['image']       = $this->crawlerData($urlRequest, 'div.book img', 'src');
    //             $data['description'] = $this->crawlerData($urlRequest, 'div.desc-text');
    //         }
    //

    public function viewImage()
    {

    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use DB;

class CommentController extends Controller
{
    public function getComments($story_id) 
    {
        try{
            $comments = Comment::where('story_id', $story_id)->get()->toArray();
            dd($comments);

        }catch(Exception $e) {
           return $this->respond(503, 'Error', []);
        }
        
        return $this->respond(200, 'OK', $comments);
    }


    public function store(Request $request) 
    {
        DB::beginTransaction();
        try{
            $comments = new Comment();
            $comments->user_id = $request->input('user_id');
            $comments->story_id = $request->input('story_id');
            $comments->content = $request->input('content');
            $comments->save();
            DB::commit();

        }catch(Exception $e) {
            DB::rollBack();
           return $this->respond(503, 'Error', []);
        }
        
        return $this->respond(200, 'OK', []);
    }
}

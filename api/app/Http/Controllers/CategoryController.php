<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use App\Models\Story;
use DB;
use Illuminate\Http\Request;
use Str;
use Weidner\Goutte\GoutteFacade;

class CategoryController extends Controller
{

    public function index()
    {
        try {
            $items = Category::all()->toArray();
        } catch (\PDOException $e) {
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
        }
        return $this->respond(200, 'OK', $items);
    }

    /**
     * create new category
     *
     * @param  Request $request
     * @return Illuminate\Response
     */

    public function store(CreateCategoryRequest $request)
    {
        DB::beginTransaction();
        try {
            $category          = $request->validated();
            $category['alias'] = Str::slug($category['name']);
            Category::updateOrCreate($category);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
            // Log::log("Error message" . $e->getMessage());
        }
        return $this->respond(200, 'OK', $category);
    }

    /**
     * crawler mutile categories
     *
     * @param  Request $request
     * @return Illuminate\Response
     */

    public function storeAll(Request $request)
    {
        DB::beginTransaction();
        try {
            $crawler = GoutteFacade::request('GET', $request->url);
            $crawler->filter('div.col-truyen-side .list-cat .row .col-xs-6')->each(function ($node) {
                Category::updateOrCreate(
                    [
                        'name'  => $this->crawlerData($node, 'a'),
                        'alias' => Str::slug($this->crawlerData($node, 'a')),
                    ]
                );
            });
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
            // Log::log("Error message" . $e->getMessage());
        }
        return $this->respond(200, 'OK', []);
    }

    /**
     * update catetory
     * @param  Request $request
     * @return Illuminate\Response
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            // Category::find($id);
            $category          = $request->validated();
            $category['alias'] = Str::slug($category['name']);
            Category::updateOrCreate($category);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
            // Log::log("Error message" . $e->getMessage());
        }
        return $this->respond(200, 'OK', $category);
    }

    public function getListOfStoriesByCategory($id)
    {
        $items = Story::where('category_id', $id)->get()->toArray();
        return $this->respond(200, 'OK', $items);
    }

    public function destroy($id)
    {
        try {
            $category = Category::findOrFail($id);
            if (!empty($category)) {
                $category->delete();
            }

        } catch (Exception $e) {
            return $this->respond(503, 'NG', ['message' => $e->getMessage()]);
        }
        return $this->respond(200, 'OK', []);
    }

}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required',
            'alias'       => 'nullable',
            'keyword'     => 'nullable',
            'description' => 'nullable',
        ];
    }

    public function message()
    {
        return [
            'required' => '::attribute không được để trống',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Thể loại',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            Controller::respond(422, [
                    'message' => 'failed validation.',
                    'errors'  => $validator->errors()->toArray()
                ]
            )
        );
    }
}

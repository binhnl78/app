<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Chapter;

class Story extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'name',
        'alias',
        'author',
        'source',
        'image',
        'rating',
        'reviews',
        'status',
        'intro',
        'description'
    ];

    public function chapter()
    {
        return $this->hasMany(Chapter::class, 'story_id', 'id');
    }
}

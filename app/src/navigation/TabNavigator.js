import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { getFocusedRouteNameFromRoute } from "@react-navigation/native";

import HomeScreen from "../screens/HomeScreen";
import CartScreen from "../screens/CartScreen";
import FavoriteScreen from "../screens/FavoriteScreen";
import StoryDetailsScreen from "../screens/StoryDetailsScreen";

import Ionicons from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import SearchScreen from "../screens/SearchScreen";
import SettingsScreen from "../screens/SettingsScreen"
import CategoryScreen from "../screens/CategoryScreen";
import SearchDetailsScreen from "../screens/SearchDetailsScreen";
import ChapterScreen from "../screens/ChapterScreen";
import StoryScreen from "../screens/StoryScreen";
import CustomDataTable from "../components/CustomDataTable";
import ViewCategory from "../screens/Admin/Story/Category/list";


const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="StoryDetails"
        component={StoryDetailsScreen}
        options={({ route }) => ({
          title: route.params?.data.name,
          headerShown: false,
        })}
        screenOptions={{headerShown: false,}}
      />
      <Stack.Screen
        name="Chapter"
        component={ChapterScreen}
        options={({ route }) => ({
          title: route.params?.data.name,
          headerShown: false,
        })}
        screenOptions={{headerShown: false,}}
      />
      
    </Stack.Navigator>
  );
};


const SearchStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Search1"
        component={SearchScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SearchDetails"
        component={SearchDetailsScreen}
        screenOptions={{headerShown: false,}}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="StoryDetails"
        component={StoryDetailsScreen}
        options={({ route }) => ({
          title: route.params?.data.name,
          headerShown: false,
        })}
        screenOptions={{headerShown: false,}}
      />
       <Stack.Screen
        name="Chapters"
        component={ChapterScreen}
        options={({ route }) => ({
          title: route.params?.data.name,
          headerShown: false,
        })}
        screenOptions={{headerShown: false,}}
      />
    </Stack.Navigator>
  );
};

const CategoryStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="CategoryScreen"
        component={CategoryScreen}
        options={{ headerShown: false }}
      />
       <Stack.Screen
        name="StoryCategory"
        component={StoryScreen}
        options={({ route }) => ({
          title: route.params?.data.name,
          headerShown: false,
        })}
        screenOptions={{headerShown: false,}}
      />
    </Stack.Navigator>
  );
};


const SettingsStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Setting1"
        component={SettingsScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="settings-outline" color={color} size={size} />
          ),
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="ViewCategory"
        component={ViewCategory}
        screenOptions={{headerShown: false,}}
      />
    </Stack.Navigator>
  );
};


const TabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: { backgroundColor: "#000" },
        tabBarInactiveTintColor: "#fff",
        tabBarActiveTintColor: "#5DA7DB",
        // tabBarVisible: false
      }}
    >
      <Tab.Screen
        name="Home2"
        component={HomeStack}
        options={({ route }) => ({
          tabBarStyle: {
            display: getTabBarVisibility(route),
            backgroundColor: "#000",
          },
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="home-outline" color={color} size={size} />
          ),
        })}
      />
      <Tab.Screen
        name="Search"
        component={SearchStack}
        options={{
          tabBarBadgeStyle: { backgroundColor: "yellow" },
          tabBarIcon: ({ color, size }) => (
            <Feather name="search" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Category"
        component={CategoryStack}
        options={{
          // tabBarBadge: 3,
          // tabBarBadgeStyle: { backgroundColor: "red" },
          tabBarIcon: ({ color, size }) => (
            <FontAwesome name="th" color={color} size={size} />
          ),
        }}
      />
      {/* <Tab.Screen
        name="Favorite"
        component={FavoriteScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="heart-outline" color={color} size={size} />
          ),
        }}
      /> */}
      {/* Setting */}
      <Tab.Screen
        name="Setting"
        component={SettingsStack}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="settings-outline" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const getTabBarVisibility = (route) => {
  // console.log(route);
  const routeName = getFocusedRouteNameFromRoute(route) ?? "Feed";
  // console.log(routeName);

  if (routeName == "StoryDetails") {
    return "none";
  }
  return "flex";
};

export default TabNavigator;

import { View, Text, ActivityIndicator } from "react-native";
import React, { useContext } from "react";
import AuthStack from "./AuthStack";
import AppStack from "./AppStack";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { AuthContext } from "../context/AuthContext";
import ViewStory from "../test/ViewStory";
import TestChapter from "../test/TestChapter";
import TryMsgScreen from "../components/Comment";
import Profile from "../test/Profile";
import CreateStory from "../screens/Admin/Story/create";
import TabViewComponent from "../components/TabViewComponent";
import CustomModal from "../components/CustomModal";
import CustomStar from "../components/CustomStar";
import CustomDataTable from "../components/CustomDataTable";
import SettingsTest from "../test/Settt";


const AppNav = () => {
  const { isLoading, userToken } = useContext(AuthContext);

  if (isLoading) {
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <ActivityIndicator size={"large"} />
    </View>;
  }
  return (
    <NavigationContainer>
      {userToken != null ? <AppStack /> : <AuthStack />}
      {/* <AppStack /> */}
      {/* <ViewStory /> */}
      {/* <TestChapter /> */}
      {/* <TryMsgScreen /> */}
      {/* <CustomDataTable /> */}
    </NavigationContainer>
  );
};

export default AppNav;

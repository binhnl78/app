import React from "react";
import {
  View,
  Text,
  TextInput,
  SafeAreaView,
  ScrollView,
  TouchableWithoutFeedback,
  Image,
} from "react-native";
import Feather from "react-native-vector-icons/Feather";
import TagSearch from "../components/TagSearch";
import { windowWidth } from "../utils/Dimensions";

const SearchScreen = ({navigation}) => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#000" }}>
      <View
        style={{
          position: "relative",
          top: 10,
          zIndex: 99,
          borderBottomWidth: 2,
          borderBottomColor: "#ccc",
          paddingBottom: 20,
          paddingHorizontal: 10,
          paddingVertical: 30,
        }}
      >
        <Text
          style={{
            fontSize: 20,
            fontWeight: "700",
            marginBottom: 6,
            color: "#fff",
          }}
        >
          Tìm kiếm
        </Text>
        <TouchableWithoutFeedback onPress={() => navigation.navigate("SearchDetails")} style={{zIndex: 999}}
        >
          <View
            style={{
              flexDirection: "row",
              borderColor: "#C6C6C6",
              borderWidth: 1,
              borderRadius: 50,
              paddingHorizontal: 10,
              paddingVertical: 6,
              alignItems: "center",
              backgroundColor: "#B2B2B2",
            }}
          >
            <Feather
              name="search"
              size={20}
              color="#fff"
              style={{ marginRight: 5 }}
            />
            <TextInput
              placeholder="Tìm truyện hoặc tác giả"
              placeholderTextColor={"#ccc"}
              color={"#ccc"}
              underlineColorAndroid="transparent"
              pointerEvents="none"
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
      <ScrollView
        style={{ position: "relative", top: 10, paddingHorizontal: 10 }}
      >
        <TagSearch />
      </ScrollView>
    </SafeAreaView>
  );
};

export default SearchScreen;

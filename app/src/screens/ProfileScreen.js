import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  Image,
  Button,
  TextInput,
  StyleSheet,
  TouchableNativeFeedback,
} from "react-native";
import { BASE_URL } from "../config";
import * as ImagePicker from "expo-image-picker";
import { AddStory } from "../context/test/CreateStory";
import { TouchableHighlight } from "react-native-gesture-handler";
import { windowHeight } from "../utils/Dimensions";
import CreateStory from "./Admin/Story/create";
import { Picker } from "react-native-form-component";
import { StatusBar } from "expo-status-bar";
import Ionicons from "react-native-vector-icons/Ionicons";
import { update } from "../model/user";
import { AuthContext } from "../context/AuthContext";
import { validateEmail } from "../utils/Validate";

const ProfileScreen = ({ navigation, route }) => {
  const [userInfo, setUserInfo] = useState([]);
  const [image, setImage] = useState(null);
  const [avatar, setAvatar] = useState(null);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");
  const { logout } = useContext(AuthContext);
  const { userToken } = useContext(AuthContext);
  // const [widthY, setWidthY] = useState(200);
  // const [heightY, setHeightY] = useState(200);
  const [role, setRole] = useState(1);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [3, 5],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      // let res = await AddStory({
      //   uri: result.uri,
      //   name: "avatar.jpg",
      //   type: "image/jpeg",
      // });
      setAvatar({
        uri: result.uri,
        name: "avatar.jpg",
        type: "image/jpeg",
      });
      setImage(result.uri);
      // console.log(res);
      // if (JSON.parse(res.code) == 200) {
      //   setImage(res.items.image);
      //   console.log(res.items.image);
      // }
      // console.log("vao", result);
      // let width_i =  windowWidth * 0.8 / result.width;
      // let height_i = windowHeight * 0.5 / result.height;
      // let ratio = Math.min(width_i, height_i);
      // setImage(result.uri);
      // setWidthY(result.width * ratio);
      // setHeightY(result.height * ratio);
      // console.log(result.width * ratio);
    }
  };

  const Submit = async () => {
    if (name.length <= 6) {
      setError("Tên đăng nhập phải chứa ít nhất 6 kí tự");
    } else if (email == "") {
      setError("Bạn chưa nhập thông tin email");
    } else if (!validateEmail(email)) {
      setError("Sai định dạng email mời nhập lại");
    } else {
      setError("");
      const res = await update(userInfo.id, name, email, avatar, role, userToken);
      // console.log(res);
      if (res.code == 200) {
        logout();
      }else {
        setError("error");
      }
    }
  };

  const isLoggedIn = async () => {
    try {
      let data = await AsyncStorage.getItem("userInfo");
      if (data) {
        let user = JSON.parse(data);
        setName(user.name);
        setEmail(user.email);
        setRole(user.role);
        setUserInfo(JSON.parse(data));
      }
    } catch (e) {}
  };
  useEffect(() => {
    isLoggedIn();
  }, []);

  return (
    <>
      <View style={{ position: "relative", zIndex: 99 }}>
        <TouchableNativeFeedback onPress={() => navigation.goBack()}>
          <View
            style={{
              paddingVertical: 5,
              position: "relative",
              left: 0,
              top: 20,
              zIndex: 999,
              paddingHorizontal: 10,
            }}
          >
            <Ionicons
              name="arrow-back"
              size={28}
              color="#C6C6C6"
              style={{ color: "#ccc" }}
            />
          </View>
        </TouchableNativeFeedback>
      </View>
      <View style={styles.container}>
        <View style={styles.avatar}>
          <TouchableNativeFeedback onPress={pickImage}>
            {image ? (
              <Image
                source={{ uri: `${image}` }}
                style={{ width: 100, height: 100, borderRadius: 50 }}
                resizeMode={"cover"}
              />
            ) : (
              <Image
                source={require("../assets/images/user-profile.jpg")}
                style={{ width: 100, height: 100, borderRadius: 50 }}
                resizeMode={"cover"}
              />
            )}
          </TouchableNativeFeedback>
        </View>
        {/* <Text style={{ color: "#000" }}>{userInfo ? userInfo.role : ""}</Text> */}

        <View style={{ width: "100%", padding: 20 }}>
          {/* <CreateStory /> */}
          <View style={{ marginBottom: 20 }}>
            <Text style={styles.title_pro}>Username</Text>
            <TextInput
              value={name}
              onChangeText={setName}
              placeholder="Username..."
              style={{
                height: 40,
                borderWidth: 1,
                paddingHorizontal: 10,
                borderRadius: 5,
              }}
            />
            <Text style={styles.title_pro}>Email</Text>
            <TextInput
              value={email}
              onChangeText={setEmail}
              placeholder="Email..."
              style={{
                height: 40,
                borderWidth: 1,
                paddingHorizontal: 10,
                borderRadius: 5,
              }}
            />
            <Text style={{color: "red", textAlign: "center"}}>{error && error}</Text>
          </View>
          <Picker
            items={[
              { label: "Admin", value: 0 },
              { label: "User", value: 1 },
            ]}
            style={{ borderWidth: 1, borderColor: "#ccc", color: "#000" }}
            label="Select Role"
            selectedValue={role}
            onSelection={(item) => setRole(item.value)}
          />
          <Button title="Lưu" onPress={Submit} />
        </View>
        <StatusBar style="light" />
      </View>
    </>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // alignItems: "center",
  },
  avatar: {
    // position: "relative",
    top: -40,
    height: windowHeight / 3,
    alignItems: "center",
    width: "100%",
    backgroundColor: "#ccc",
    paddingTop: 60,
  },
  title_pro: {
    // color: "#ccc",
    // borderBottomLeftRadius: 5,
    marginBottom: 10,
  },
});

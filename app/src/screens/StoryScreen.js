import React, { useContext, useEffect, useState } from "react";
import { View, Text, ScrollView, TouchableNativeFeedback } from "react-native";
import CustomList from "../components/CustomList";
import { show } from "../model/category";
import Ionicons from "react-native-vector-icons/Ionicons";
import { AuthContext } from "../context/AuthContext";

const StoryScreen = ({ navigation, route }) => {
  const [data, setData] = useState([]);
  const [storiesTab, setStoriesTab] = useState(1);
  const { userToken } = useContext(AuthContext);

  const getStoryByCategory = async () => {
    const res = await show(route.params.data.id, userToken);
    if (res.code == 200) {
      console.log(res.items);
      setData(res.items ?? []);
    }
  };

  useEffect(() => {
    getStoryByCategory();
  }, []);

  return (
    <ScrollView style={{ padding: 10 }}>
      <TouchableNativeFeedback onPress={() => navigation.goBack()}>
        <View
          style={{
            paddingVertical: 5,
            position: "relative",
            left: 0,
            top: 20,
            zIndex: 999,
            paddingHorizontal: 10,
          }}
        >
          <Ionicons
            name="arrow-back"
            size={28}
            color="#C6C6C6"
            style={{ color: "#ccc"}}
          />
        </View>
      </TouchableNativeFeedback>
      <View style={{marginTop: 16}}>
      {storiesTab == 1 &&
        data.map((item) => (
          <CustomList
            key={item.id}
            photo={item.image}
            title={item.alias}
            subTitle={item.name}
            isFree={item.status}
            source={item.source}
            created_at={item.created_at}
            author={item.author}
            onPress={() =>
              navigation.navigate("StoryDetails", {
                data: item,
              })
            }
          />
        ))}
      </View>
    </ScrollView>
  );
};

export default StoryScreen;

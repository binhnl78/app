import React, { useState, useContext, useEffect } from "react";
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  ImageBackground,
  TextInput,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StyleSheet,
  FlatList,
} from "react-native";
import Carousel from "react-native-snap-carousel";
import Feather from "react-native-vector-icons/Feather";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import BannerSlider from "../components/BannerSlider";
import { windowWidth } from "../utils/Dimensions";
import { freeGames, paidGames, sliderData } from "../model/data";
import CustomSwitch from "../components/CustomSwitch";
import ListItem from "../components/ListItem";
import { story_hot, story_news } from "../model/story";
import { AuthContext } from "../context/AuthContext";
import { BASE_URL } from "../config";
import Loader from "../components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

export default function HomeScreen({ navigation }) {
  const [storiesTab, setStoriesTab] = useState(1);
  const { userInfo } = useContext(AuthContext) ? useContext(AuthContext) : "";
  const [carousel, setCarousel] = useState(null);
  const [data, setData] = useState([]);
  const [storyNews, setStoryNews] = useState([]);
  const { userToken } = useContext(AuthContext);

  const renderBanner = ({ item, index }) => {
    return <BannerSlider data={item} />;
  };

  const onSelectSwitch = (value) => {
    setStoriesTab(value);
  };

  const getStory = async () => {
    // console.log(userToken)
    const res = await story_hot(userToken);
    const res1 = await story_news(userToken);
    if (res.code == 200) {
      // console.log(res.items);
      setData(res.items ?? []);
    }

    if (res1.code == 200) {
      console.log(res1.items);
      setStoryNews(res1.items ?? []);
    }
  };

  useEffect(() => {
    getStory();
  }, []);

  const sliderData1 = [
    {
      title: "First Game",
      image: require("../assets/images/homescreen/game-1.jpeg"),
    },
    {
      title: "Second Game",
      image: require("../assets/images/homescreen/game-2.jpeg"),
    },
    {
      title: "Third Game",
      image: require("../assets/images/homescreen/game-3.png"),
    },
  ];

  const flatl = [
    {
      id: "1",
      title: "First Game",
      image: require("../assets/images/homescreen/game-1.jpeg"),
    },
    {
      id: "2",
      title: "Second Game",
      image: require("../assets/images/homescreen/game-2.jpeg"),
    },
    {
      id: "3",
      title: "Third Game",
      image: require("../assets/images/homescreen/game-3.png"),
    },
    {
      id: "4",
      title: "Third Game1",
      image: require("../assets/images/asphalt-9.jpeg"),
    },
    {
      id: "5",
      title: "Third Game2",
      image: require("../assets/images/FarCry6.png"),
    },
    {
      id: "6",
      title: "Third Game3",
      image: require("../assets/images/genshin-impact.jpeg"),
    },
  ];

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#000" }}>
      {/* <Loader loading={loading} /> */}
      <ScrollView style={{ padding: 20 }}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "center",
            marginBottom: 20,
            borderBottomWidth: 2,
            borderBottomColor: "#ccc",
            paddingVertical: 12,
          }}
        >
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Image
                source={require("../assets/images/user-profile.jpg")}
                style={{ width: 35, height: 35, borderRadius: 25 }}
              />
            {/* {userInfo ? (
              <Image
                source={{ uri: `${BASE_URL}/images/${userInfo.avatar}` }}
                style={{ width: 35, height: 35, borderRadius: 25 }}
              />
            ) : (
              <Image
                source={require("../assets/images/user-profile.jpg")}
                style={{ width: 35, height: 35, borderRadius: 25 }}
              />
            )} */}
          </TouchableOpacity>
          {userInfo && (
            <Text
              style={{
                fontSize: 18,
                fontFamily: "Roboto-Medium",
                color: "#fff",
                marginLeft: 4,
              }}
            >
              {userInfo.name}
            </Text>
          )}
        </View>

        {/* <View
          style={{
            flexDirection: "row",
            borderColor: "#C6C6C6",
            borderWidth: 1,
            borderRadius: 8,
            paddingHorizontal: 10,
            paddingVertical: 8,
          }}
        >
          <Feather
            name="search"
            size={20}
            color="#C6C6C6"
            style={{ marginRight: 5 }}
          />
          <TextInput placeholder="Search" />
        </View> */}

        {/* <View
          style={{
            marginVertical: 15,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={{ fontSize: 18, color: "#fff" }}>Mới Nhất</Text>
          <TouchableOpacity onPress={() => {}}>
            <Text style={{ color: "#0aada8" }}>Tất cả</Text>
          </TouchableOpacity>
        </View> */}

        <Carousel
          layout={"default"}
          ref={(c) => {
            setCarousel(c);
            // this._carousel = c;
          }}
          data={sliderData}
          renderItem={renderBanner}
          sliderWidth={windowWidth - 40}
          itemWidth={windowWidth - 50}
          loop={true}
        />

        <View style={{ marginVertical: 20 }}>
          {/* <CustomSwitch
            selectionMode={1}
            option1="Miễn phí"
            option2="Trả phí"
            onSelectSwitch={onSelectSwitch}
          /> */}
        </View>
        <Text style={styles.title}>Truyện HOT</Text>
        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
          {storiesTab == 1 &&
            data.map((item) => (
              <ListItem
                key={item.id}
                photo={item.image}
                title={item.alias}
                subTitle={item.name}
                isFree={item.status}
                onPress={() =>
                  navigation.navigate("StoryDetails", {
                    data: item,
                  })
                }
              />
            ))}
          {/* {storiesTab == 2 &&
            paidGames.map((item) => (
              <ListItem
                key={item.id}
                photo={item.poster}
                title={item.title}
                subTitle={item.subtitle}
                isFree={item.isFree}
                price={item.price}
                onPress={() =>
                  navigation.navigate("StoryDetails", {
                    title: item.title,
                    id: item.id,
                  })
                }
              />
            ))} */}
        </View>

        <Text style={styles.title}>Trailer truyện mới nè</Text>
        <View>
          <Image
            source={require("../assets/images/homescreen/im1.jpg")}
            style={{ width: "100%", height: 150, borderRadius: 5 }}
            resizeMode={"contain"}
          />
          <Text style={{ color: "#fff", fontSize: 12 }}>
            Ăn mày vươn mình thành nữ hoàng , hôm nay lật thẻ chọn phu quân nào
            đây?
          </Text>
          <Text style={{ color: "#ccc", fontSize: 10 }}>
            Ăn mày vươn mình thành nữ hoàng , hôm nay lật thẻ chọn phu quân nào
            đây? sáng 11h 11/11/2022
          </Text>
        </View>

        <Text style={styles.title}>Truyện Tranh Hot</Text>
        <View style={{ flexDirection: "row", width: "100%" }}>
          <Image
            source={require("../assets/images/homescreen/im2.jpg")}
            style={{ width: "40%", height: 166, borderRadius: 5 }}
            // resizeMode={"contain"}
          />
          <View style={{ padding: 20, width: "60%" }}>
            <Text
              style={{ color: "#fff", textAlign: "left" }}
              ellipsizeMode="tail"
              numberOfLines={2}
            >
              Đệ Tử Của Ta Đều Siêu Thần
            </Text>
            <Text
              style={{
                width: "100%",
                color: "#ccc",
                fontSize: 13,
                marginTop: 10,
                textAlign: "justify",
              }}
              ellipsizeMode="tail"
              numberOfLines={4}
            >
              Truyện tiên hiệp thường kể về quá trình tu luyện và khám phá thế
              giới tu sĩ thần tiên đầy bí ẩn của nhân vật chính. Trong truyện
              tiên hiệp thường chia ra những cấp bậc tu luyện trước khi thành
              tiên như sau:
            </Text>
          </View>
        </View>

        <Text style={styles.title}>Mới nhất</Text>
        <View style={{ marginBottom: 30 }}>
          <FlatList
            data={storyNews}
            renderItem={flat}
            keyExtractor={(key) => key.id}
            horizontal
          />
        </View>

        <Text style={styles.title}>Danh sách khác</Text>
        <View style={{ width: "100%" }}>
          <View
            style={{
              flexDirection: "row",
              alignContent: "space-between",
              alignItems: "center",
              borderBottomWidth: 0.5,
              borderColor: "#ccc",
              paddingRight: 20,
            }}
          >
            {/* <TouchableWithoutFeedback> */}
            <Text
              style={{
                width: "100%",
                color: "#fff",
                paddingVertical: 10,
              }}
            >
              Ngôn Tình Cố Mạn Full
            </Text>
            <Feather
              name="chevron-right"
              size={20}
              color="#C6C6C6"
              style={{ marginRight: 5 }}
            />
            {/* </TouchableWithoutFeedback> */}
          </View>
        </View>
        <View style={{ width: "100%" }}>
          <View
            style={{
              flexDirection: "row",
              alignContent: "space-between",
              alignItems: "center",
              borderBottomWidth: 0.5,
              borderColor: "#ccc",
              paddingRight: 20,
            }}
          >
            {/* <TouchableWithoutFeedback> */}
            <Text
              style={{
                width: "100%",
                color: "#fff",
                paddingVertical: 10,
              }}
            >
              Ngôn Tình Cố Mạn Full
            </Text>
            <Feather
              name="chevron-right"
              size={20}
              color="#C6C6C6"
              style={{ marginRight: 5 }}
            />
            {/* </TouchableWithoutFeedback> */}
          </View>
        </View>
        <View style={{ width: "100%" }}>
          <View
            style={{
              flexDirection: "row",
              alignContent: "space-between",
              alignItems: "center",
              borderBottomWidth: 0.5,
              borderColor: "#ccc",
              paddingRight: 20,
            }}
          >
            {/* <TouchableWithoutFeedback> */}
            <Text
              style={{
                width: "100%",
                color: "#fff",
                paddingVertical: 10,
              }}
            >
              Ngôn Tình Cố Mạn Full
            </Text>
            <Feather
              name="chevron-right"
              size={20}
              color="#C6C6C6"
              style={{ marginRight: 5 }}
            />
            {/* </TouchableWithoutFeedback> */}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const flat = ({ item }) => {
  return (
    <TouchableWithoutFeedback
      onPress={() =>
        navigation.navigate("StoryDetails", {
          data: item,
        })
      }
    >
      <View>
        <Image
          source={{ uri: item.image }}
          style={{
            width: windowWidth / 4,
            height: 150,
            borderRadius: 5,
            marginRight: 10,
          }}
          resizeMode={"cover"}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  title: {
    color: "#fff",
    // fontFamily: "",
    fontSize: 16,
    paddingVertical: 10,
  },
});

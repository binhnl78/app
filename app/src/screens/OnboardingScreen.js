import React, {useCallback} from 'react'
import { SafeAreaView, View, Text, TouchableOpacity, Image, ImageBackground } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useFonts } from 'expo-font';
import Gaming from '../assets/images/misc/gaming.svg';
import {windowWidth} from '../utils/Dimensions';

const OnboardingScreen = ({navigation}) => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
      }}>
      <View style={{marginTop: 20}}>
        <Text
          style={{
            fontFamily: 'Inter-Bold',
            // fontWeight: 'bold',
            fontSize: 30,
            color: '#20315f',
          }}>
          Truyện Full
        </Text>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      {/* <Gaming
          width={300}
          height={300}
          style={{transform: [{rotate: '-15deg'}]}}
        /> */}
        <Image source={require('../../assets/manga-toon.jpg')} resizeMode="center" width={windowWidth} />
      </View>
      <TouchableOpacity
        style={{
          backgroundColor: '#AD40AF',
          padding: 20,
          width: '90%',
          borderRadius: 10,
          marginBottom: 50,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}
        onPress={() => navigation.navigate('Login')}>
        <Text
          style={{
            color: 'white',
            fontSize: 18,
            textAlign: 'center',
            // fontWeight: 'bold',
            fontFamily: 'Roboto-BoldItalic',
          }}>
          Bắt đầu
        </Text>
        <Ionicons name="arrow-forward" size={22} color="#fff" />
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default OnboardingScreen;

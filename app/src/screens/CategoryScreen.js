import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  SafeAreaView,
  TouchableWithoutFeedback,
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import Loader from "../components/Loader";
import { AuthContext } from "../context/AuthContext";
import { category } from "../model/category";
import { windowWidth } from "../utils/Dimensions";

const CategoryScreen = ({ navigation }) => {
  const [data, setData] = useState([]);
  const [isLoading, setisLoading] = useState(true);
  const [status, setstatus] = useState(0);
  // const [loading, setLoading] = useState(true);
  const { userToken } = useContext(AuthContext);

  const getDataByCategory = async () => {
    const res = await category(userToken);
    if (res.code == 200) {
      // console.log(res.items);
      setData(res.items ?? []);
      setisLoading(false);
    }
  };

  useEffect(() => {
    getDataByCategory();
  }, []);

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#000" }}>
      <Loader loading={isLoading} />
      <Text
        style={{
          color: "#fff",
          fontSize: 20,
          padding: 10,
          textAlign: "center",
        }}
      >
        Thể Loại
      </Text>
      <ScrollView style={{ padding: 10 }}>
        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
          {data != null &&
            data.map((item) => (
              <CategoryList
                name={item.name}
                key={item.id}
                // photo={item.poster}
                // title={item.title}
                // subTitle={item.subtitle}
                // isFree={item.isFree}
                onPress={() =>
                  navigation.navigate("StoryCategory", {
                    data: item,
                  })
                }
              />
            ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const CategoryList = ({ name, onPress }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View
        style={{
          width: (windowWidth - 50) / 2,
          borderColor: "#fff",
          borderWidth: 1,
          marginRight: 10,
          paddingVertical: 14,
          borderRadius: 5,
          marginBottom: 10,
        }}
      >
        <Text
          style={{
            color: "#fff",
            fontSize: 16,
            textAlign: "center",
          }}
        >
          {name}
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default CategoryScreen;

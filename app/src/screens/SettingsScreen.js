import {
  Entypo,
  Ionicons,
  MaterialIcons,
  EvilIcons,
  AntDesign,
  FontAwesome,
} from "@expo/vector-icons";
import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Appearance,
  TouchableOpacity,
} from "react-native";

const SettingsScreen = ({ navigation }) => {
  const colorScheme = Appearance.getColorScheme();
  console.log(colorScheme);
  if (colorScheme === "dark") {
    // Use dark color scheme
  }
  return (
    <View style={styles.container}>
      <View style={styles.actionBar}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons name="arrow-back-outline" size={30} />
        </TouchableOpacity>
        <Text
          style={{
            flex: 1,
            fontSize: 20,
            fontWeight: "600",
            textAlign: "center",
            marginRight: 30,
          }}
        >
          Setting
        </Text>
      </View>
      <View style={styles.list}>
        <TouchableOpacity>
          <View style={styles.item}>
            <EvilIcons name="user" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>
              Thông tin cá nhân
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("ViewCategory")}>
          <View style={styles.item}>
            <EvilIcons name="retweet" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>
              Cập nhật dữ liệu web
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("ViewCategory")}>
          <View style={styles.item}>
            <FontAwesome name="list-alt" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>
              Quản lý thể loại
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("ViewCategory")}>
          <View style={styles.item}>
            <FontAwesome name="book" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>Quản lý truyện</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("ViewCategory")}>
          <View style={styles.item}>
            <FontAwesome name="user" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>
              Quản thông tin người dùng
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.item}>
            <Entypo name="language" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>Language</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.item}>
            <MaterialIcons name="feedback" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>Feedback</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.item}>
            <AntDesign name="exclamationcircleo" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>About</Text>
          </View>
        </TouchableOpacity>
      </View>
      <StatusBar />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    backgroundColor: "#ccc"
  },
  actionBar: {
    width: "100%",
    height: 70,
    backgroundColor: "#80bfff",
    flexDirection: "row",
    alignItems: "center",
  },
  list: {},
  item: {
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: "grey",
    height: 60,
    paddingLeft: 10,
  },
});
export default SettingsScreen;
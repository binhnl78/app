import React, {useEffect, useState} from "react";
import { View, SafeAreaView, ScrollView } from "react-native";
import CustomDataTable from "../../../../components/CustomDataTable";
import { category } from "../../../../model/category";

const ViewCategory = () => {
    [data, setData] = useState("");

    const getCategory = async () => {
        const res = await category();
        if (res.code == 200) {
          setData(res.items ?? []);
        }
      };
    
      useEffect(() => {
        getCategory();
      }, []);
    


    return (
        <ScrollView>
            <CustomDataTable data={data} />
        </ScrollView>
    );
}

export default ViewCategory;
import React, { useState } from "react";
import reactDom from "react-dom";
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  StyleSheet,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  FlatList,
} from "react-native";
import CustomBtn from "../components/CustomBtn";
import TestChapter from "../test/TestChapter";
import { windowHeight, windowWidth } from "../utils/Dimensions";
import Ionicons from "react-native-vector-icons/Ionicons";
import AntDesign from "react-native-vector-icons/AntDesign";
import Fontisto from "react-native-vector-icons/Fontisto";
import Entypo from "react-native-vector-icons/Entypo";
import TryMsgScreen from "../components/Comment";
import { onShare } from "../components/CustomShare";
import CustomStar from "../components/CustomStar";
import Loader from "../components/Loader";

const StoryDetailsScreen = ({ navigation, route }) => {
  const [storiesTab, setStoriesTab] = useState(1);
  const [modal, setModal] = useState(false);
  const onSelectSwitch = (value) => {
    setStoriesTab(value);
  };

  const ViewC = (data) => {
    // const [modal, setModal] = useState(false);
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginLeft: 10,
            paddingVertical: 10,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginRight: 10,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginRight: 10,
              }}
            >
              <Fontisto
                name="fire"
                size={12}
                color="yellow"
                style={{ marginRight: 3 }}
              />
              <Text style={{ color: "#ccc" }}>641.4M</Text>
            </View>
            <AntDesign
              name="like1"
              size={12}
              color="red"
              style={{ marginRight: 3 }}
            />
            <Text style={{ color: "#ccc" }}>{data.reviews}M</Text>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <AntDesign
              name="star"
              size={12}
              color="yellow"
              style={{ marginRight: 3 }}
            />
            <Text style={{ color: "#ccc", marginRight: 2 }}>{data.rating}</Text>
            <TouchableWithoutFeedback
              onPress={() => {
                setModal(true);
              }}
            >
              <Entypo
                name="new-message"
                size={12}
                color="white"
                style={{ marginRight: 3 }}
              />
            </TouchableWithoutFeedback>
          </View>
        </View>
        <Text style={{ color: "#ccc", marginLeft: 10 }}>
          Tác giả: {data.author}
        </Text>
        <Text style={{ color: "#ccc", marginLeft: 10 }}>
          Nguồn: {data.source}
        </Text>
        <Text
          style={{
            color: "red",
            marginLeft: 10,
            fontSize: 16,
            fontWeight: "700",
          }}
        >
          {data.status == 1 && "Đã full"}
        </Text>
        <View style={styles.content}>
          <Text style={{ color: "#ccc" }}>{data.description}</Text>
        </View>

        <View style={styles.content}>
          <Text style={{ color: "#fff" }}>Bình luận nổi bật</Text>
          <View>
            <TryMsgScreen id={route.params.id}/>
          </View>
        </View>
        {modal && <CustomStar />}
      </View>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#181818" }}>
      <View
        style={{
          position: "relative",
          width: "100%",
          top: 20,
          flexDirection: "row",
          alignItems: "center",
          alignContent: "space-around",
          paddingHorizontal: 10,
          zIndex: 99,
        }}
      >
        <View style={{ marginRight: windowWidth / 1.7 }}>
          <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
            <Ionicons
              name="arrow-back"
              size={28}
              color="#C6C6C6"
              style={{ color: "#ccc" }}
            />
          </TouchableWithoutFeedback>
        </View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            alignContent: "space-between",
            height: 45,
          }}
        >
          <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
            <AntDesign
              name="download"
              size={24}
              color="#C6C6C6"
              style={{ color: "#ccc", marginRight: 12 }}
            />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={onShare}>
            <AntDesign
              name="sharealt"
              size={24}
              color="#C6C6C6"
              style={{ color: "#ccc", marginRight: 12 }}
            />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
            <AntDesign
              name="ellipsis1"
              size={24}
              color="#C6C6C6"
              style={{ color: "#ccc", marginRight: 12 }}
            />
          </TouchableWithoutFeedback>
        </View>
      </View>
      <ImageBackground
        source={{ uri: route.params?.data.image }}
        style={{
          width: "100%",
          height: windowHeight / 3,
        }}
        resizeMode="contain"
      >
        <View
          style={{ padding: 10, bottom: 5, position: "absolute", bottom: 5 }}
        >
          <Text style={{ color: "#fff", fontSize: 18, marginBottom: 5 }}>
            {route.params?.data.name}
          </Text>
        </View>
      </ImageBackground>
      <View>
        <CustomBtn
          selectionMode={1}
          option1="Chi tiết"
          option2="Chapters"
          onSelectSwitch={onSelectSwitch}
        />
      </View>
      <ScrollView>{storiesTab == 1 && ViewC(route.params?.data)}</ScrollView>
      <View style={styles.content}>
        {storiesTab == 2 && <TestChapter id={route.params?.data.id} />}
      </View>
      {/* <Loader loading={true} /> */}
    </SafeAreaView>
  );
};

const ViewA = (id) => {
  return (
    <View>
      <FlatList />
    </View>
  );
};

export default StoryDetailsScreen;

const styles = StyleSheet.create({
  content: {
    padding: 10,
    bottom: 5,
  },
  title: {
    color: "red",
    fontSize: 14,
    fontWeight: "700",
    textAlign: "center",
    paddingVertical: 10,
    backgroundColor: "#181818",
  },
  col: {
    width: "50%",
  },
});

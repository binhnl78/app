import React, { useState, useEffect, useContext } from "react";

import {
  SafeAreaView,
  Text,
  StyleSheet,
  View,
  FlatList,
  TextInput,
  TouchableNativeFeedback,
} from "react-native";
import { BASE_URL } from "../config";
import Ionicons from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";
import { AuthContext } from "../context/AuthContext";

const SearchDetailsScreen = ({ navigation }) => {
  const [search, setSearch] = useState("");
  const [status, setStatus] = useState(false);
  const [filteredDataSource, setFilteredDataSource] = useState([]);
  const [masterDataSource, setMasterDataSource] = useState([]);
  const { userToken } = useContext(AuthContext);

  useEffect(() => {
    fetch(`${BASE_URL}/api/story`, {
      headers: {
        // "Content-type": "application/json",
        "Authorization": `Bearer ${userToken}`,
    }
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setFilteredDataSource(responseJson.items);
        setMasterDataSource(responseJson.items);
        // console.log(responseJson.items);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const searchFilterFunction = (text) => {
    if (text) {
      const newData = masterDataSource.filter(function (item) {
        const itemData = item.name ? item.name.toUpperCase() : "".toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      setFilteredDataSource(masterDataSource);
      setSearch(text);
    }
  };

  const ItemView = ({ item }) => {
    return (
      <Text style={styles.itemStyle} onPress={() => getItem(item)}>
        {item.id}
        {"."}
        {item.name}
      </Text>
    );
  };

  const ItemSeparatorView = () => {
    return (
      <View
        style={{
          height: 0.5,
          width: "100%",
          backgroundColor: "#C8C8C8",
        }}
      />
    );
  };

  const getItem = (item) =>
    navigation.navigate("StoryDetails", {
      data: item,
    });

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TouchableNativeFeedback onPress={() => navigation.goBack()}>
        <View
          style={{
            paddingVertical: 5,
            position: "relative",
            left: 0,
            top: 20,
            zIndex: 999,
            paddingHorizontal: 10,
            width: 50
          }}
        >
          <Ionicons
            name="arrow-back"
            size={28}
            color="#C6C6C6"
            style={{ color: "#ccc" }}
          />
        </View>
      </TouchableNativeFeedback>
      <View style={styles.container}>
        <View
          style={{
            flexDirection: "row",
            borderColor: "#C6C6C6",
            borderWidth: 1,
            borderRadius: 50,
            paddingHorizontal: 10,
            paddingVertical: 4,
            alignItems: "center",
            backgroundColor: "#B2B2B2",
          }}
        >
          <Feather
            name="search"
            size={20}
            color="#fff"
            style={{ marginRight: 5 }}
          />
          <TextInput
            onChangeText={(text) => {
              searchFilterFunction(text);
              setStatus(true);
            }}
            value={search}
            underlineColorAndroid="transparent"
            placeholder="Tìm kiếm truyện"
            placeholderTextColor={"#ccc"}
            color={"#ccc"}
          />
        </View>
        {status && (
          <FlatList
            data={filteredDataSource}
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={ItemSeparatorView}
            renderItem={ItemView}
          />
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    backgroundColor: "white",
    paddingHorizontal: 10
  },
  itemStyle: {
    padding: 10,
  },
  textInputStyle: {
    height: 40,
    borderWidth: 1,
    paddingLeft: 20,
    margin: 5,
    borderColor: "#009688",
    backgroundColor: "#FFFFFF",
  },
});

export default SearchDetailsScreen;

import React from "react";
import { View, Text, TouchableWithoutFeedback, Image } from "react-native";
import { windowWidth } from "../utils/Dimensions";

const TagSearch = () => {
  return (
    <View>
      <Text style={{ color: "#fff", paddingVertical: 14, fontSize: 16 }}>
        Khám phá các tag
      </Text>
      <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
        <TouchableWithoutFeedback>
          <View
            style={{
              position: "relative",
              width: windowWidth / 2 - 20,
              height: 80,
              marginBottom: 10,
              marginRight: 10,
            }}
          >
            <Image
              source={require("../assets/images/Altos-Odyssey.jpeg")}
              style={{ width: "100%", height: "100%", borderRadius: 5 }}
              resizeMode={"stretch"}
            />
            <Text
              style={{
                color: "#fff",
                fontSize: 16,
                position: "absolute",
                top: "40%",
                left: 6,
              }}
            >
              Bí ẩn
            </Text>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback>
          <View
            style={{
              position: "relative",
              width: windowWidth / 2 - 20,
              height: 80,
              marginBottom: 10,
              marginRight: 10,
            }}
          >
            <Image
              source={require("../assets/images/FarCry6.png")}
              style={{ width: "100%", height: "100%", borderRadius: 5 }}
              resizeMode={"stretch"}
            />
            <Text
              style={{
                color: "#fff",
                fontSize: 16,
                position: "absolute",
                top: "40%",
                left: 6,
              }}
            >
              Bí ẩn
            </Text>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback>
          <View
            style={{
              position: "relative",
              width: windowWidth / 2 - 20,
              height: 80,
              marginBottom: 10,
              marginRight: 10,
            }}
          >
            <Image
              source={require("../assets/images/fortnite.webp")}
              style={{ width: "100%", height: "100%", borderRadius: 5 }}
              resizeMode={"stretch"}
            />
            <Text
              style={{
                color: "#fff",
                fontSize: 16,
                position: "absolute",
                top: "40%",
                left: 6,
              }}
            >
              Bí ẩn
            </Text>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback>
          <View
            style={{
              position: "relative",
              width: windowWidth / 2 - 20,
              height: 80,
              marginBottom: 10,
              marginRight: 10,
            }}
          >
            <Image
              source={require("../assets/images/genshin-impact.jpeg")}
              style={{ width: "100%", height: "100%", borderRadius: 5 }}
              resizeMode={"stretch"}
            />
            <Text
              style={{
                color: "#fff",
                fontSize: 16,
                position: "absolute",
                top: "40%",
                left: 6,
              }}
            >
              Bí ẩn
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};

export default TagSearch;

import React, { useState, useRef } from "react";
import { StyleSheet, View, TextInput, Button, Text } from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";

function TryMsgScreen() {
  const [Comments, SetComments] = useState([]);
  const [commentValue, setCommentValue] = useState("");
  const [showComment, setShowComment] = useState(false);
  const InputRef = useRef();

  const AddToComments = () => {
    let temp = {
      id: GenerateUniqueID(),
      commentValue: commentValue,
    };
    SetComments([...Comments, temp]);
    InputRef.current.clear();
  };

  const GenerateUniqueID = () => {
    return Math.floor(Math.random() * Date.now()).toString();
  };

  return (
    <View style={styles.container}>
      {/* <FontAwesome name="commenting" size={34} color="black" /> */}
      {Comments.map((c) => (
        <View style={styles.showComment_container} key={c.id}>
          <Text>{c.commentValue}</Text>
        </View>
      ))}
      <View style={styles.comment_container}>
        <TextInput
          style={styles.input_txt}
          onChangeText={(text) => setCommentValue(text)}
          placeholder="Bình luận ..."
          placeholderTextColor={"#ccc"}
          ref={InputRef}
        />
        <Button title="Gửi" onPress={() => AddToComments()} />
      </View>

    </View>
  );
}

export default TryMsgScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  comment_container: {
    width: "100%",
    minHeight: 50,
    flexDirection: "row",
    marginTop: 30,
    marginBottom: 40,
  },
  input_txt: {
    width: "86%",
    borderWidth: 1,
    color: "#ccc",
    borderColor: "#ccc",
    padding: 10,
  },
  showComment_container: {
    width: "98%",
    minHeight: 50,
    backgroundColor: "#B0C4DE",
    marginTop: 10,
  },
});

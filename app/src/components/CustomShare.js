import React from 'react';
import { Share, View, Button } from 'react-native';

export const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          'Đọc truyện với ứng dụng Managan',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
      }
    } catch (error) {
      alert(error.message);
    }
  };

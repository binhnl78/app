import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { windowWidth } from "../utils/Dimensions";

export default function CustomList({
  photo,
  title,
  subTitle,
  isFree,
  price,
  source,
  created_at,
  author,
  onPress,
}) {
  const [dateValues, timeValues] = created_at.split(' ');
  return (
    <View style={{marginTop: 20, width: windowWidth - 100}}>
      <TouchableOpacity onPress={onPress}>
        <View style={{ width: "100%", flexDirection: "row" }}>
          <Image
            source={{ uri: photo }}
            style={{width: 100, height: 150, borderRadius: 10 }}
            resizeMode={"cover"}
          />
          <View style={{padding: 10}}>
            <Text
              style={{
                color: "#000",
                fontSize: 14,
                fontWeight: "700",
                paddingHorizontal: 4,
                paddingVertical: 4,
              }}
              ellipsizeMode="tail"
              numberOfLines={2}
            >
              {subTitle}
            </Text>
            <Text style={{fontWeight: "bold"}}>Tác giả: {author ? author : ""}</Text>
            <Text>Nguồn: {source ? source : ""}</Text>
            <Text>Ngày tạo: {dateValues ? dateValues : ""}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

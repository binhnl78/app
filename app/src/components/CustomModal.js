import React, { useState } from "react";
import { Alert, Modal, StyleSheet, Text, Pressable, View, ScrollView } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import { windowHeight } from "../utils/Dimensions";

const CustomModal = ({ data, status }) => {
  const [modalVisible, setModalVisible] = useState(true);
  return (
    <ScrollView >
      <Modal
        animationType="slide"
        transparent={true}
        visible={status}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={{backgroundColor: "#ccc",  marginVertical: 8, padding: 10, marginHorizontal: 50, borderRadius: 10, height: windowHeight - 50}}>
          <Pressable
            onPress={() => setModalVisible(!modalVisible)}
          >
            <AntDesign name="close" size={20} color="#fff" style={{textAlign: "right"}} />
          </Pressable>
          <ScrollView>
            <Text style={{fontWeight: "700", fontSize: 16}}>{data.name}</Text>
            <Text style={{marginTop: 10}}>
                {data.content}
            </Text>
          </ScrollView>
        </View>
      </Modal>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
});

export default CustomModal;

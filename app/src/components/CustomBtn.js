import React, {useState} from 'react';
import {View, Text,StyleSheet ,TouchableOpacity} from 'react-native';

export default function CustomBtn({
  selectionMode,
  option1,
  option2,
  onSelectSwitch,
}) {
  const [getSelectionMode, setSelectionMode] = useState(selectionMode);

  const updateSwitchData = value => {
    setSelectionMode(value);
    onSelectSwitch(value);
  };

  const color = 
    {
      btn : "#fff",
    }
  

  return (
    <View
    style={{
      flexDirection: "row",
      alignItems: "center",
      alignContent: "space-around",
      width: "100%",
      borderBottomWidth: 1,
      borderColor: "#fff",
      zIndex: 999,
    }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => updateSwitchData(1)}
        style={{
          flex: 1,
          // backgroundColor: getSelectionMode == 1 ? color.btn : '#e4e4e4',
          backgroundColor: "#181818",
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            color: getSelectionMode == 1 ? 'red' : color.btn,
            fontSize: 14,
            fontFamily: 'Roboto-Medium',
            paddingVertical: 10,
          }}>
          {option1}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => updateSwitchData(2)}
        style={{
          flex: 1,
          // backgroundColor: getSelectionMode == 2 ? color.btn : '#e4e4e4',
          backgroundColor: "#181818",
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            color: getSelectionMode == 2 ? 'red' : color.btn,
            fontSize: 14,
            fontFamily: 'Roboto-Medium',
            paddingVertical: 10,
          }}>
          {option2}
        </Text>
      </TouchableOpacity>
    </View>
  );
}


const styles = StyleSheet.create({
  content: {
    padding: 10,
    bottom: 5,
  },
  title: {
    color: "red",
    fontSize: 14,
    fontWeight: "700",
    textAlign: "center",
    paddingVertical: 10,
    backgroundColor: "#181818",
  },
  col: {
    width: "50%",
  },
});

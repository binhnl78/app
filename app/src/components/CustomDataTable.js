import * as React from "react";
import { View, Text, Modal } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { DataTable } from "react-native-paper";
import AntDesign from "react-native-vector-icons/AntDesign";
import { delete_category } from "../model/category";
import { windowHeight } from "../utils/Dimensions";
const optionsPerPage = [2, 3, 4];

const CustomDataTable = ({ data }) => {
  const [page, setPage] = React.useState(3);
  const [modalVisible, setModalVisible] = React.useState(false);
  const [itemsPerPage, setItemsPerPage] = React.useState(optionsPerPage[0]);

  React.useEffect(() => {
    setPage(3);
  }, [itemsPerPage]);

  const deleteCategory = async (id) => {
    setModalVisible(true);
    // const res = await delete_category(id);
    // if(res.code) {
    //     alert("Xóa thành công");
    // }
  };

  const TableRow = (row) => {
    // console.log(row);
    return (
      <DataTable.Row key={row.id}>
        <DataTable.Cell>{row.id}</DataTable.Cell>
        <DataTable.Cell>{row.name}</DataTable.Cell>
        <DataTable.Cell>{row.description}</DataTable.Cell>
        <DataTable.Cell>
          <TouchableOpacity
            onPress={() => {
              deleteCategory(row.id);
            }}
          >
            <AntDesign
              name="delete"
              size={20}
              color="#FF6666"
              style={{ marginRight: 5 }}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}}>
            <AntDesign
              name="edit"
              size={20}
              color="#5F9DF7"
              style={{ marginRight: 5 }}
            />
          </TouchableOpacity>
        </DataTable.Cell>
      </DataTable.Row>
    );
  };

  return (
    <>
      <Text
        style={{
          marginTop: 30,
          textAlign: "center",
          fontSize: 16,
          fontWeight: "900",
        }}
      >
        Danh sách thể loại
      </Text>
      <DataTable>
        <DataTable.Header>
          <DataTable.Title>Id</DataTable.Title>
          <DataTable.Title>Thể loại</DataTable.Title>
          <DataTable.Title>Mô tả</DataTable.Title>
          <DataTable.Title>Action</DataTable.Title>
        </DataTable.Header>

        {data
          ? data.map((item) => (
              <TableRow
                key={item.id}
                id={item.id}
                name={item.name}
                description={item.description}
              />
            ))
          : ""}

        <DataTable.Pagination
          page={page}
          numberOfPages={3}
          onPageChange={(page) => setPage(page)}
          label="1-2 of 6"
          optionsPerPage={optionsPerPage}
          itemsPerPage={itemsPerPage}
          setItemsPerPage={setItemsPerPage}
          showFastPagination
          optionsLabel={"Rows per page"}
        />
      </DataTable>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View
          style={{
            marginTop: 100,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#ccc",
            marginVertical: 8,
            padding: 10,
            height: 150,
            width: "50%",
            marginHorizontal: "30%",
          }}
        >
          <Text>Xóa thành công</Text>
          <TouchableOpacity
            onPress={() => {
              setModalVisible(!modalVisible);
            }}
          >
            <Text
              style={{ padding: 10, backgroundColor: "#fff", marginTop: 20 }}
            >
              OK
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </>
    // </View>
  );
};

export default CustomDataTable;

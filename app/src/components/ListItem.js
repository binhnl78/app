import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { windowWidth } from "../utils/Dimensions";

export default function ListItem({
  photo,
  title,
  subTitle,
  isFree,
  price,
  onPress,
}) {
  return (
    <View
      style={{
        flexWrap: "wrap",
        flexDirection:'row',
        // justifyContent: 'space-between',
        // alignItems: 'center',
        // overflow: "scroll",
        marginBottom: 20,
      }}
    >
      <TouchableOpacity style={{width: (windowWidth - 64) / 3, marginRight: 8}} onPress={onPress}>
        <View style={{width: "100%" }}>
          <Image
            source={{uri: photo}}
            style={{ width: "100%", height: 150, borderRadius: 10 }}
            resizeMode={"cover"}
          />
          <View>
            <Text
              style={{
                width: "100%",
                color: "#fff",
                fontFamily: "Roboto-Medium",
                fontSize: 12,
                paddingHorizontal: 4,
                paddingVertical: 4
              }}
              ellipsizeMode='tail' numberOfLines={2}
            >
              {subTitle}
            </Text>
            {/* <Text
              numberOfLines={1}
              style={{
                color: "#333",
                fontFamily: "Roboto-Medium",
                fontSize: 14,
                textTransform: "uppercase",
              }}
            >
              {title}
            </Text> */}
          </View>
        </View>
      </TouchableOpacity>

      {/* <TouchableOpacity onPress={onPress} style={{
        backgroundColor:'#0aada8',
        padding:10,
        width: 100,
        borderRadius: 10,
      }}>
        <Text style={{
          color: '#fff',
          textAlign: 'center',
          fontFamily: 'Roboto-Medium',
          fontSize: 14,
        }}>
          {isFree == 'Yes' && 'Reading'}
          {isFree == 'No' && price}
        </Text>
      </TouchableOpacity> */}
    </View>
  );
}

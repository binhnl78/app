import {
  Entypo,
  Ionicons,
  MaterialIcons,
  EvilIcons,
  AntDesign,
  FontAwesome,
} from "@expo/vector-icons";
import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Appearance,
  TouchableOpacity,
} from "react-native";

const SettingsScreen = ({ navigation }) => {
  const colorScheme = Appearance.getColorScheme();
  console.log(colorScheme);
  if (colorScheme === "dark") {
    // Use dark color scheme
  }
  return (
    <View style={styles.container}>
      <View style={styles.actionBar}>
        <TouchableOpacity>
          <Ionicons name="arrow-back-outline" size={30} />
        </TouchableOpacity>
        <Text
          style={{
            flex: 1,
            fontSize: 20,
            fontWeight: "600",
            textAlign: "center",
            marginRight: 30,
          }}
        >
          Setting
        </Text>
      </View>
      <View style={styles.list}>
        <TouchableOpacity>
          <View style={styles.item}>
            <EvilIcons name="user" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>
              Thông tin cá nhân
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("ViewCategory")}>
          <View style={styles.item}>
            <EvilIcons name="retweet" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>
              Cập nhật dữ liệu web
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("ViewCategory")}>
          <View style={styles.item}>
            <FontAwesome name="list-alt" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>
              Quản lý thể loại
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("ViewCategory")}>
          <View style={styles.item}>
            <FontAwesome name="book" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>Quản lý truyện</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("ViewCategory")}>
          <View style={styles.item}>
            <FontAwesome name="user" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>
              Quản thông tin người dùng
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.item}>
            <Entypo name="language" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>Language</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.item}>
            <MaterialIcons name="feedback" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>Feedback</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.item}>
            <AntDesign name="exclamationcircleo" size={30} />
            <Text style={{ fontSize: 15, marginLeft: 10 }}>About</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
  },
  actionBar: {
    width: "100%",
    height: 70,
    backgroundColor: "#80bfff",
    flexDirection: "row",
    alignItems: "center",
  },
  list: {},
  item: {
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: "grey",
    height: 60,
    paddingLeft: 10,
  },
});
export default SettingsScreen;




// import { StatusBar } from "expo-status-bar";
// import React from "react";
// import {
//   View,
//   Text,
//   StyleSheet,
//   TouchableWithoutFeedback,
//   Appearance,
// } from "react-native";

// const SettingsScreen = ({navigation}) => {
//   const colorScheme = Appearance.getColorScheme();
//   console.log(colorScheme)
//   if (colorScheme === "dark") {
//     // Use dark color scheme
//   }
//   return (
//     <View style={[styles.container]}>
//       <View style={[styles.list]}>
//         <TouchableWithoutFeedback>
//           <Text>Update dữ liệu mới</Text>
//         </TouchableWithoutFeedback>
//       </View>

//       <View style={[styles.list]}>
//         <TouchableWithoutFeedback>
//           <Text>Quản thông tin người dùng</Text>
//         </TouchableWithoutFeedback>
//       </View>

//       <View style={[styles.list]}>
//         <TouchableWithoutFeedback onPress={() => navigation.navigate("ViewCategory")}>
//           <Text>Quản lý thể loại</Text>
//         </TouchableWithoutFeedback>
//       </View>

//       <View style={[styles.list]}>
//         <TouchableWithoutFeedback>
//           <Text>Quản lý truyện</Text>
//         </TouchableWithoutFeedback>
//       </View>
//       <StatusBar />
//     </View>
//   );
// };

// export default SettingsScreen;

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: "center",
//     alignItems: "center",
//     padding: 10,
//   },
//   list: {
//     width: "100%",
//     padding: 10,
//     marginTop: 29,
//     borderWidth: 1,
//     borderColor: "#ccc",
//   },
// });


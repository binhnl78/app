import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  Pressable,
  ScrollView,
  Modal,
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { chapterById } from "../model/chapter";
import AntDesign from "react-native-vector-icons/AntDesign";
import FontAwesome from "react-native-vector-icons/FontAwesome";
// import CustomModal from "../components/CustomModal";
// import { Alert, Modal, StyleSheet, Text, Pressable, View, ScrollView } from "react-native";
import { windowHeight } from "../utils/Dimensions";
import Loader from "../components/Loader";
import { AuthContext } from "../context/AuthContext";

const TestChapter = ({ id }) => {
  const [data, setData] = useState([]);
  const [status, setStatus] = useState(false);
  const [itemData, setItemData] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [chapter, setChapter] = useState([]);
  const [loading, setLoading] = useState(true);
  const { userToken } = useContext(AuthContext);

  const getChapter = async(id) => {
    const res = await chapterById(id, userToken);
    if (res.code == 200) {
      setData(res.items ?? []);
      setLoading(false);
    }
  };

  useEffect(() => {
    getChapter(id);
  }, []);

  const Item = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          setModalVisible(true);
          setChapter(item);
        }}
      >
        <View
          style={{
            paddingVertical: 10,
            borderBottomWidth: 1,
            borderColor: "#ccc",
          }}
        >
          <Text style={{ color: "#fff" }}>{item.name}</Text>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={{ marginRight: 10, color: "#ccc" }}>07/11/2018</Text>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <AntDesign
                name="like2"
                size={10}
                color="#ccc"
                style={{ color: "#ccc", marginRight: 12 }}
              />
              <Text style={{ marginRight: 10, color: "#ccc" }}>17018</Text>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <FontAwesome
                name="commenting-o"
                size={10}
                color="#ccc"
                style={{ color: "#ccc", marginRight: 12 }}
              />
              <Text style={{ marginRight: 10, color: "#ccc" }}>389232</Text>
            </View>
          </View>
        </View>

        <ScrollView>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              setModalVisible(!modalVisible);
            }}
          >
            <View
              style={{
                backgroundColor: "#ccc",
                padding: 10,
                marginHorizontal: 10,
                borderRadius: 10,
                height: windowHeight - 24,
                marginTop: 22
              }}
            >
              <Pressable onPress={() => setModalVisible(!modalVisible)}>
                <AntDesign
                  name="close"
                  size={20}
                  color="#fff"
                  style={{ textAlign: "right" }}
                />
              </Pressable>
              <ScrollView>
                <Text style={{ fontWeight: "700", fontSize: 16 }}>
                  {chapter.name}
                </Text>
                <Text style={{ marginTop: 10, marginBottom: 16 }}>{chapter.content}</Text>
              </ScrollView>
            </View>
          </Modal>
        </ScrollView>
      </TouchableOpacity>
    );
  };

  return (
    <View>
      <FlatList data={data} renderItem={Item} keyExtractor={(key) => key.id} />
      <Loader loading={loading} />
    </View>
  );
};

export default TestChapter;

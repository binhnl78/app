import React, { useState, useEffect } from "react";
import { Button, Image, View, Platform } from "react-native";
import * as ImagePicker from "expo-image-picker";
import { windowWidth, windowHeight } from "../utils/Dimensions";
import { AddStory } from "../context/test/CreateStory";

export default function Profile() {
  const [image, setImage] = useState(null);
  const [widthY, setWidthY] = useState(200);
  const [heightY, setHeightY] = useState(200);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [3, 5],
      quality: 1,
    });

    console.log(result);

    if (!result.canceled ) {
      let res = await AddStory({
        uri: result.assets.uri,
        name: "avatar.jpg",
        type: "image/jpeg",
      });
       console.log(res);
      // console.log("vao", result);
      // let width_i =  windowWidth * 0.8 / result.width;
      // let height_i = windowHeight * 0.5 / result.height;
      // let ratio = Math.min(width_i, height_i);
      // setImage(result.uri);
      // setWidthY(result.width * ratio);
      // setHeightY(result.height * ratio);
      // console.log(result.width * ratio);
    }
  };

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Button title="Pick an image from camera roll" onPress={pickImage} />
      {image && (
        <Image
          source={{ uri: image }}
          style={{ width: widthY ?? 200, height: heightY ?? 200 }}
          resizeMode={"contain"}
        />
      )}
      <Image
        style={{ width: 200, height: 200 }}
        source={{
          uri: "file:///var/mobile/Containers/Data/Application/4F5612D6-2099-4E59-A5AE-67C3DAC8402B/Library/Caches/ExponentExperienceData/%2540anonymous%252Fapp-07f08975-0c0b-4d6f-8d1e-3a7662cd62d0/ImagePicker/B94C9067-ADE8-471E-B619-0C3EAE8D3394.jpg",
        }}
        width={200} height={200}
      />
    </View>
  );
}

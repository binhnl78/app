
import React, { useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { BASE_URL } from "../config";


export const chapterById = async (id, userToken) => {
  token = AsyncStorage.getItem('token')
  try {
      const result = await axios.get(`${BASE_URL}/api/chapter/show/${id}`, {
        headers: {
            "Content-type": "application/json",
            "Authorization": `Bearer ${userToken}`,
        }
      })
      const { code, date, items } = result.data;
      return {
        code,
        date,
        items,
      }
    } catch (error) {
        console.log(error)
    //   return handleError(error)
    }
};




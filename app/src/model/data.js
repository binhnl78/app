import React, { useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { BASE_URL } from "../config";


export const comments = async (id, comment, userToken) => {
  const formData = new FormData();
  formData.append("id", id);
  formData.append("comment", comment);
  try {
      const result = await axios.post(`${BASE_URL}/api/story/comments/${id}`, {
        headers: {
            "Content-type": "application/json",
            "Authorization": `Bearer ${userToken}`,
        },
        formData
      })
      // console.log(result.data);
      const { code, date, items } = result.data;
      return {
        code,
        date,
        items,
      }
    } catch (error) {
        console.log(error)
      return handleError(error)
    }
};

import React, { useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { BASE_URL } from "../config";


export const story_hot = async (userToken) => {
  try {
      const result = await axios.get(`${BASE_URL}/api/story/hot`, {
        headers: {
            // "Content-type": "application/json",
            "Authorization": `Bearer ${userToken}`,
        },
      })
      // console.log(result.data);
      const { code, date, items } = result.data;
      return {
        code,
        date,
        items,
      }
    } catch (error) {
        console.log('VAo'+ userToken,error)
      // return handleError(error)
    }
};

export const story_news = async (userToken) => {
  token = AsyncStorage.getItem('token')
  try {
      const result = await axios.get(`${BASE_URL}/api/story/news`, {
        headers: {
            // "Content-type": "application/json",
            "Authorization": `Bearer ${userToken}`,
        },
      })
    //   console.log(result.data);
      const { code, date, items } = result.data;
      return {
        code,
        date,
        items,
      }
    } catch (error) {
        console.log(error)
      return handleError(error)
    }
};


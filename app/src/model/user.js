import React from "react";
import { BASE_URL } from "../config";

export const update = async (id, name, email, avatar, role, userToken) => {
  const formData = new FormData();
  formData.append("name", name);
  formData.append("email", email);
  formData.append("avatar", avatar);
  formData.append("role", role);
  try {
    const response = await fetch(`${BASE_URL}/api/user/update/${id}`, {
      method: "POST",
      headers: new Headers({
          "Content-type": "multipart/form-data",
          "Authorization": `Bearer ${userToken}`,
      }),
      body: formData
    //   headers: {
    //     Accept: "application/json",
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify({
    //     name: name,
    //     email: email,
    //     avatar: avatar,
    //     role: role,
    //   }),
    });
    const result = await response.json();
    // console.log(result);
    const { code, date, items } = result;
    return { code, date, items }
  } catch (error) {
    return handleError(error);
  }
};


export const comment = async (id, comment, ) => {
  const formData = new FormData();
  formData.append("id", id);
  formData.append("comment", comment);
  try {
    const response = await fetch(`${BASE_URL}/api/user/update/${id}`, {
      method: "POST",
      headers: new Headers({
          "Content-type": "multipart/form-data",
      }),
      body: formData
    });
    const result = await response.json();
    const { code, date, items } = result;
    return { code, date, items }
  } catch (error) {
    return handleError(error);
  }
};

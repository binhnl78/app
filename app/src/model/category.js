import React, { useContext, useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { BASE_URL } from "../config";
import { AuthContext } from "../context/AuthContext";




export const category = async (userToken) => {
  try {
      const result = await axios.get(`${BASE_URL}/api/category`, {
        headers: {
            // "Content-type": "application/json",
            "Authorization": `Bearer ${userToken}`,
        },
      })
      const { code, date, items } = result.data;
      return {
        code,
        date,
        items,
      }
    } catch (error) {
        // console.log(error)
      return handleError(error)
    }
};


export const show = async (id, userToken) => {
  try {
    
      const result = await axios.get(`${BASE_URL}/api/category/list-stories/${id}`, {
        headers: {
            // "Content-type": "application/json",
            "Authorization": `Bearer ${userToken}`,
        },
      })
      const { code, date, items } = result.data;
      return {
        code,
        date,
        items,
      }
    } catch (error) {
        // console.log(error)
      return handleError(error)
    }
};




export const delete_category = async (id, userToken) => {
  console.log(id);
  try {
      const result = await axios.delete(`${BASE_URL}/api/category/delete/${id}`, {
        headers: {
            // "Content-type": "application/json",
            "Authorization": `Bearer ${userToken}`,
        },
      })
      const { code, date, items } = result.data;
      return {
        code,
        date,
        items,
      }
    } catch (error) {
        // console.log(error)
      return handleError(error)
    }
};


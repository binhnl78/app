import React from "react"
import { BASE_URL } from "../../config";

export const Register = async (name, email, password, password_confirmation) => {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('email', email);
    formData.append('password', password);
    formData.append('password_confirmation', password_confirmation);
    // console.log(formData);
    try {
        const response = await fetch(`${BASE_URL}/api/register`, {
            method: 'POST',
            headers: new Headers({
                "Content-type": "multipart/form-data",
            }),
            body: formData
        })
        const result = await response.json()
        // console.log(result);
        // const { message, status, data } = result
        // return { message, status, data }
    } catch (error) {
        return handleError(error)
    }
}
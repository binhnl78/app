import React from "react"
import { BASE_URL } from "../../config";

export const AddStory = async (data) => {
    const formData = new FormData();
    formData.append('image', data);
    // console.log(formData);
    try {
        const response = await fetch(`${BASE_URL}/api/story/store`, {
            method: 'POST',
            headers: new Headers({
                "Content-type": "multipart/form-data",
            }),
            body: formData
        })
        const result = await response.json();
        // console.log(result);
        // const { message, status, data } = result;
        // return { message, status, data }
    } catch (error) {
        // return handleError(error)
    }
}
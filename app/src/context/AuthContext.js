import React, { createContext, useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { BASE_URL } from "../config";

export const AuthContext = createContext();

export const AuthProvider = ({children}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [userToken, setUserToken] = useState(null);
    const [userInfo, setUserInfo] = useState(null);

    const login = (email, password) => {
        setIsLoading(true);
        axios.post(`${BASE_URL}/api/login`, {
            email,
            password
        })
        .then(res => {
            console.log("vao", res.data);
            let userInfo = res.data.user;
            setUserInfo(userInfo);
            setUserToken(res.data.access_token);
           
            if (res && res.data && res.data.access_token) {
                AsyncStorage.setItem('userToken', res.data.access_token);
                AsyncStorage.setItem('userInfo', JSON.stringify(userInfo));
                console.log(userInfo);
            }
            setIsLoading(false);
        })
        .catch(e => {
             console.log(`Login error ${e}`);
        });
    }

    const logout = () => {
        setIsLoading(true);
        setUserToken(null);
        AsyncStorage.removeItem('userToken');
        AsyncStorage.removeItem('userInfo');
        setIsLoading(false);
    }

    const isLoggedIn = async() => {
        try {
            let userInfo = await AsyncStorage.getItem('userInfo');
            // console.log(AsyncStorage.getItem('userInfo'));
            let userToken = await AsyncStorage.getItem('userToken');
            userInfo = JSON.parse(userInfo);
            if( userInfo ){
                setUserToken(userToken);
                setUserInfo(userInfo);
            }
            setIsLoading(false);
            
        } catch (e) {
            console.log(`Error ${e}`);
        }
    }

    useEffect(() => {
        isLoggedIn();
    }, []);
    return (
        <AuthContext.Provider value={{login, logout, isLoading, userToken, userInfo}}>
            {children}
        </AuthContext.Provider>
    );
}
import React from "react";
import { StatusBar } from "expo-status-bar";
import { useFonts } from "expo-font";
import { AuthProvider } from "./src/context/AuthContext";
import AppNav from "./src/navigation/AppNav";

function App() {
  const [fontsLoaded] = useFonts({
    "Inter-Bold": require("./src/assets/fonts/Inter-Bold.ttf"),
    "Roboto-Bold": require("./src/assets/fonts/Roboto-Bold.ttf"),
    "Roboto-BoldItalic": require("./src/assets/fonts/Roboto-BoldItalic.ttf"),
    "AbrilFatface-Regular": require("./src/assets/fonts/AbrilFatface-Regular.ttf"),
    "GreatVibes-Regular": require("./src/assets/fonts/GreatVibes-Regular.ttf"),
    "Roboto-Italic": require("./src/assets/fonts/Roboto-Italic.ttf"),
    "Roboto-Medium": require("./src/assets/fonts/Roboto-Medium.ttf"),
    "Roboto-MediumItalic": require("./src/assets/fonts/Roboto-MediumItalic.ttf"),
    "Roboto-Regular": require("./src/assets/fonts/Roboto-Regular.ttf"),
  });

  if (!fontsLoaded) {
    return null;
  }

  return (
    <AuthProvider>
      <StatusBar  style="light"/>
      <AppNav />
    </AuthProvider>
  );
}

export default App;

#APP đọc truyện

```
CRAWLER DATA : https://truyenfull.vn
```

```
cd existing_repo
git remote add origin https://gitlab.com/binhnl78/app.git
git branch -M main
git push -uf origin main
```

```
Backend : PHP(LARAVEL)

Method	Endpoint
POST	/api/auth/register
POST	/api/auth/login
GET	/api/user-profile
POST	/api/auth/refresh
POST	/api/auth/logout
POST	user/update/{id}
GET	/api/story
GET	/api/chapter
POST    /story/store
POST    /story/all
GET	/story/get-chapter-by-story/{id}
GET	/story/search/{keyword}
GET	/story/hot
GET	/story/news
GET	/story/full-chapter
GET	/chapter
GET	/chapter/show/{id}
POST	/chapter/all
GET	category/
POST	category/
POST	category/all
GET	category/list-stories/{id}


```